package com.tests;

import java.sql.*;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

import static java.lang.System.out;

public class Database_table_creation {

    private Connection base_con;
    Hashtable<String[], String> all_catname_tags;



    public Database_table_creation(Connection a,Hashtable<String[],String> cat_tags) throws SQLException {
        this.base_con=a;
        this.all_catname_tags=cat_tags;
        String sql="";
        Statement stmt = null;
        Statement stmt2 = null;
//        cat_tags.values();
        Set<String> value_set = new HashSet<String>(cat_tags.values());
//        out.println(value_set.toString());
        DatabaseMetaData md = this.base_con.getMetaData();
        try{
            stmt2 = this.base_con.createStatement();

            sql="CREATE TABLE IF NOT EXISTS osm_all_data"+
                    "(ID TEXT PRIMARY KEY     NOT NULL," +
                    " NAME           TEXT    , " +
                    " FLAG           int    , " +
                    " CATEGORY       TEXT    )";

            stmt2.executeUpdate(sql);
            ResultSet rs = md.getColumns(null, null, "osm_all_data", "geom_");
            if (!rs.next()) {
                //Column in table exist
                sql="SELECT AddGeometryColumn('public','osm_all_data','geom_',4326,'GEOMETRY',2);";
//                    out.println(sql);

                stmt2.executeQuery(sql);
            }

            sql="CREATE TABLE IF NOT EXISTS yago_all_data"+
                    "(ID TEXT PRIMARY KEY     NOT NULL," +
                    " NAME               TEXT, " +
                    " name_updated       TEXT, " +
                    " CATEGORY           TEXT, " +
                    " category_updated   TEXT, " +
                    " LINK               TEXT, " +
                    " GEOMETRY           TEXT, " +
                    " FLAG               INT)";

            stmt2.executeUpdate(sql);
            ResultSet rs2 = md.getColumns(null, null, "yago_all_data", "geom_prev");
            if (!rs2.next()) {
                //Column in table exist
                sql="SELECT AddGeometryColumn('public','yago_all_data','geom_prev',4326,'GEOMETRY',2);";
//                    out.println(sql);

                stmt2.executeQuery(sql);
            }
            rs2 = md.getColumns(null, null, "yago_all_data", "geom_updated");
            if (!rs2.next()) {
                //Column in table exist
                sql="SELECT AddGeometryColumn('public','yago_all_data','geom_updated',4326,'GEOMETRY',2);";
//                    out.println(sql);

                stmt2.executeQuery(sql);
            }
            //YAGO_GEOMETRY TABLE
            sql="CREATE TABLE IF NOT EXISTS yago_geos"+
                    "(ID TEXT PRIMARY KEY     NOT NULL)";
            stmt2.executeUpdate(sql);
            rs2 = md.getColumns(null, null, "yago_geos", "geom_");
            if (!rs2.next()) {
                //Column in table exist
                sql="SELECT AddGeometryColumn('public','yago_geos','geom_',4326,'GEOMETRY',2);";
                stmt2.executeQuery(sql);
            }
            stmt2.close();

        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(11);
        }
        for(String osm_name:value_set){
            try{
                stmt = this.base_con.createStatement();

//            String sql1 = "CREATE TABLE IF NOT EXISTS COMPANY " +
//                    "(ID INT PRIMARY KEY     NOT NULL," +
//                    " NAME           TEXT    NOT NULL, " +
//                    " AGE            INT     NOT NULL, " +
//                    " ADDRESS        CHAR(50), " +
//                    " SALARY         REAL)";
//            out.println("SQL1= "+sql1);
                sql="CREATE TABLE IF NOT EXISTS "+osm_name.toLowerCase()+
                        "(ID TEXT PRIMARY KEY     NOT NULL," +
                        " NAME           TEXT    , " +
                        " FLAG           int    , " +
                        " GEOMETRY       TEXT    )";

                stmt.executeUpdate(sql);
                ResultSet rs = md.getColumns(null, null, osm_name.toLowerCase(), "geom_");
                if (!rs.next()) {
                    //Column in table exist
                    sql="SELECT AddGeometryColumn('public','"+osm_name.toLowerCase()+"','geom_',4326,'GEOMETRY',2);";
//                    out.println(sql);

                    stmt.executeQuery(sql);
                }

                //YAGO TABLES
                sql="CREATE TABLE IF NOT EXISTS "+osm_name.toLowerCase()+"_yago"+
                        "(ID TEXT PRIMARY KEY     NOT NULL," +
                        " NAME           TEXT    , " +
                        " LINK           TEXT    , " +
                        " GEOMETRY       TEXT    )";

                stmt.executeUpdate(sql);
                ResultSet rs2 = md.getColumns(null, null, osm_name.toLowerCase()+"_yago", "geom_prev");
                if (!rs2.next()) {
                    //Column in table exist
                    sql="SELECT AddGeometryColumn('public','"+osm_name.toLowerCase()+"_yago"+"','geom_prev',4326,'GEOMETRY',2);";
//                    out.println(sql);

                    stmt.executeQuery(sql);
                }
                rs2 = md.getColumns(null, null, osm_name.toLowerCase()+"_yago", "geom_updated");
                if (!rs2.next()) {
                    //Column in table exist
                    sql="SELECT AddGeometryColumn('public','"+osm_name.toLowerCase()+"_yago"+"','geom_updated',4326,'GEOMETRY',2);";
//                    out.println(sql);

                    stmt.executeQuery(sql);
                }

                stmt.close();


//                Statement stmt2 = base_con.createStatement();
//                String sql2 = "INSERT INTO "+osm_name+" (ID,NAME,GEOMETRY) "
//                        + "VALUES (1, 'παραλία', 'tetragono' ) ON CONFLICT(ID) DO UPDATE SET NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY;";
//                stmt.executeUpdate(sql2);
//                sql2 = "INSERT INTO "+osm_name+" (ID,NAME,GEOMETRY) "
//                        + "VALUES (2, 'παραλία', 'tetragono' ) ON CONFLICT(NAME);";
//                stmt.executeUpdate(sql2);
            }
            catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getClass().getName()+": "+e.getMessage());
                System.exit(11);
            }
        }

    }



}
