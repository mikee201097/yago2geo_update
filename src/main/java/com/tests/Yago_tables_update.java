package com.tests;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class Yago_tables_update {

    private Connection base_con;

    Yago_tables_update(Connection a, Hashtable<String[],String> cat_tags) throws SQLException {
        this.base_con=a;
        Statement stmt = this.base_con.createStatement();
        Set<String> value_set = new HashSet<String>(cat_tags.values());
        String sql="UPDATE yago_all_data SET geom_updated=(CASE WHEN (ST_Equals(yago_all_data.geom_prev,osm_all_data.geom_))THEN null ELSE osm_all_data.geom_ END),"+
                "name_updated=(CASE WHEN (yago_all_data.name=CONCAT('\"',osm_all_data.name,'\"'))THEN null ELSE osm_all_data.name END),"+
                "category_updated=(CASE WHEN (yago_all_data.category=osm_all_data.category)THEN null ELSE osm_all_data.category END) "
                +"FROM osm_all_data WHERE osm_all_data.id=yago_all_data.id AND osm_all_data.flag=1 ;";
//        sql="UPDATE yago_all_data SET geom_updated= osm_all_data.geom_ ,"+
//                "name_updated=osm_all_data.name ,"+
//                "category_updated=osm_all_data.category  "
//                +"FROM osm_all_data WHERE yago_all_data.id=osm_all_data.id AND osm_all_data.flag=1 ;";
        stmt.executeUpdate(sql);
//        for(String osm_name:value_set){
//            String sql="UPDATE "+osm_name+"_yago SET geom_updated=(SELECT geom_ FROM "+osm_name+" WHERE "+osm_name+".id="+osm_name+"_yago.id AND "+osm_name+".flag=1);";
//            stmt.executeUpdate(sql);
//
//        }
        stmt.close();
    }
}
