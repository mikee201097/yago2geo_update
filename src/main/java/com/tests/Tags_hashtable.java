package com.tests;

import org.openstreetmap.osmosis.core.domain.v0_6.Tag;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class Tags_hashtable {
    private String name;
    private String Custom_name=null;
    private Hashtable<String, Tags_hashtable> h1=null;
//    private int osm_id;


    public Tags_hashtable(List<String> list ,String inner_name) {
        this.name=list.get(0);
        list.remove(0);
        if(list.isEmpty()){
//            if(h1==null){
//                h1=new Hashtable<String, Tags_hashtable>();
////                System.out.println("GRAFEI TO "+this.name);
//            }
            System.out.println("GRAFEI TO CUSTOM NAME: "+inner_name+" STO "+this.name);
            this.Custom_name=inner_name;
        }
        else{
//            this.name=list.get(0);
            if(h1==null){
                h1=new Hashtable<String, Tags_hashtable>();
                System.out.println("GRAFEI TO "+this.name);
            }
            String child_name=list.get(0);
            Tags_hashtable inner_hashtable=new Tags_hashtable(list, inner_name);
//            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
//            inner_hashtable.print_hash();
//            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            h1.put(child_name,inner_hashtable);
        }
    }

    public Tags_hashtable(Hashtable<String[], String> all_catname_tags){
//        String split_chars2="[&]";
        if(h1==null){
            h1=new Hashtable<String, Tags_hashtable>();
        }
        for(String[] str:all_catname_tags.keySet()){
//            if(str.length==1){
//                this.name=str[0];

                List<String> list = new ArrayList<String>(Arrays.asList(str));
//                list= Arrays.asList(str);
                System.out.println("PRIN "+list.toString());
//                this.Custom_name=all_catname_tags.get(str);
                int i=0;
                Hashtable<String, Tags_hashtable> temp=h1;
                while(temp.containsKey(str[i])){
                    temp=temp.get(str[i]).getH1();
                    list.remove(0);
                    i++;
                }
                String node_name=list.get(0);
                System.out.println("META "+list.toString());
                Tags_hashtable inner_hashtable=new Tags_hashtable(list, all_catname_tags.get(str));
                temp.put(node_name,inner_hashtable);

//            }
//            else if(str.length>1){
//                List<String> list = new ArrayList<String>();
//                this.name=str[0];
//                list= Arrays.asList(str);
//                list.remove(0);
//                if()
//
//                Tags_hashtable inner_hashtable=new Tags_hashtable(list, all_catname_tags.get(str) );
//            }
        }
    }

    public String getName() {
        return name;
    }

    public String getCustom_name() {
        return Custom_name;
    }

    public Hashtable<String, Tags_hashtable> getH1() {
        return h1;
    }

    public void print_hash(){
        for(String key:this.h1.keySet() ){
            System.out.println("Key: "+key);
            if(h1.get(key).getCustom_name()!=null){
                System.out.println(" Value: "+h1.get(key).getCustom_name());
            }
            Hashtable<String, Tags_hashtable> temp=h1;
            if(temp.get(key).getH1()!=null){
                temp.get(key).print_hash();
            }

//            while(temp.get(key).getH1()!=null){
//                temp.get(key).print_hash();
//                temp=temp.get(key).getH1();
//            }
            System.out.println("_____________");
        }
    }



    public String hash_table_Search(List<Tag> Tag_list,String until_now_custom){
        List<Tag> temp_list=Tag_list;
        String cust_name=until_now_custom;
        if(this.Custom_name!=null){
            cust_name=this.Custom_name;
        }
        for(Tag tag: Tag_list){
            String tag_format=tag.getKey()+":"+tag.getValue();
            if(h1!=null){
                if(h1.containsKey(tag_format)){
                    temp_list.remove(tag);
                    cust_name=this.h1.get(tag_format).hash_table_Search(temp_list,cust_name);
//                    String currnet_custom=h1.get(tag_format).getCustom_name();
//                    if(currnet_custom!=null){
//                        cust_name=currnet_custom;
//                    }
                    break;
                }
            }
            else{
                if(this.Custom_name==null){
                    return until_now_custom;
                }
                else{
                    return this.Custom_name;
                }
            }

        }

        return cust_name;
    }
}


