package com.tests;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import static java.lang.System.out;

public class Input_file_reader {
    private String input_file_name;
    private File file;
    private String split_chars="[, =\t]";
    private String base_name;
    private String base_url;
    private String base_user;
    private String base_password;
    LinkedHashMap<String, String> conf_info;
    Hashtable<String[],String> all_tags;

    public Input_file_reader(String file_path) throws Exception {
        this.input_file_name=file_path;
        this.conf_info = new LinkedHashMap<String, String>();
        this.file=new File(file_path);
        this.all_tags=new Hashtable<String[], String>();
        String st="";
        BufferedReader br_input_file = new BufferedReader(new FileReader(file));

        while ((st = br_input_file.readLine()) != null){
            int i=0;
            String trimmed_st=st.trim();
//            System.out.println("AAAAAA"+trimmed_st+"a");
            if(trimmed_st.equals("")){
                continue;
            }
            if(trimmed_st.charAt(0)=='#'){
                continue;
            }
            //st.split("= ");

            String[] st_array=st.split(this.split_chars);
            if(st_array[0].equalsIgnoreCase("base_name")){
                this.conf_info.put("base_name",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("base_url")){
                this.conf_info.put("base_url",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("base_user")){
                this.conf_info.put("base_user",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("base_password")){
                this.conf_info.put("base_password",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("Osm_pbf_path"))
            {
                this.conf_info.put("Osm_pbf_path",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("osm2psql_style_path"))
            {
                this.conf_info.put("osm2psql_style_path",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("osmuppdate_dir"))
            {
                this.conf_info.put("osmuppdate_dir",st_array[1]);
            }
            else if(st_array[0].equalsIgnoreCase("yago2geoFile"))
            {
                this.conf_info.put("yago2geoFile",st_array[1]);
            }
            else if(st_array[0].equals("TAG_START_HERE")){
                break;
            }
//            System.out.println(Arrays.toString(st_array));

//            if(st_array[0].equals("classification_path")){
//                classification_path=st_array[1];
//            }
//
//            out.println(classification_path);
        }
        String name_category=null;
        while ((st = br_input_file.readLine()) != null){
            int i=0;

            String split_chars2="[| =\t]";
            String trimmed_st=st.trim();
//            System.out.println("AAAAAA"+trimmed_st+"a");
            if(trimmed_st.equals("")){
                continue;
            }
            if(trimmed_st.charAt(0)=='#'){
//                name_category=trimmed_st.substring(1);
                continue;
            }
            if(trimmed_st.charAt(0)=='!'){
                name_category=trimmed_st.substring(1);
                continue;
            }
            //st.split("= ");
            st=st.replace(" ","");

            String[] st_array=st.split(split_chars2);
//            this.all_tags.put(st_array,name_category);
            System.out.println(Arrays.toString(st_array));
            for(String str:st_array){
                String split_chars3="[&]";
                String[] st_array2=str.split(split_chars3);
                this.all_tags.put(st_array2,name_category);
            }


//          this.all_tags.put(name_category,st_array);
        }
    }

    public LinkedHashMap<String, String> get_base_info(){
        return this.conf_info;
    }

    public Hashtable<String[], String> get_all_tags() {
        return all_tags;
    }
}
