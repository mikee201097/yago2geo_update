package com.tests;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.util.GeometryCombiner;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.operation.linemerge.LineMerger;
import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.container.v0_6.NodeContainer;
import org.openstreetmap.osmosis.core.container.v0_6.RelationContainer;
import org.openstreetmap.osmosis.core.container.v0_6.WayContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.*;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;

import crosby.binary.osmosis.OsmosisReader;
import org.openstreetmap.osmosis.pbf2.v0_6.PbfReader;

import static java.lang.System.out;

/**
 * Receives data from the Osmosis pipeline and prints ways which have the
 * 'highway key.
 *
 * @author pa5cal
 */
public class MyOsmReader implements Sink {

    private Hashtable<String[], String> all_tags_name;
    private Tags_hashtable multi_level_hash;
    private Connection base_connection;
    ExecutorService executor;
    Statement stmt = null,stmt2 = null;


    public int collection_members=0;
    public int collection_members_found=0;
    public int collection_members_with_more_than_1_id=0;

    public MyOsmReader(Hashtable<String[], String> a,Tags_hashtable multi_hash,Connection con,int workers,ExecutorService exec){

        this.all_tags_name=a;
        this.multi_level_hash=multi_hash;
        this.base_connection=con;
        this.executor = exec;

        try {
            this.stmt = base_connection.createStatement();
            this.stmt2 = base_connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    @Override
    public void initialize(Map<String, Object> arg0) {
    }
    public String find_name_tag(Collection<Tag> tag_list){
        String node_name=null;
        for ( Tag myTag : tag_list) {
            if ( myTag.getKey().equalsIgnoreCase("name")){
                node_name=myTag.getValue();
                if(node_name!=null){
                    node_name=node_name.replace("\'","\'\'");
                }
                return node_name;
            }
        }
         return node_name;
    }

    public String Insert_sql(String table,String id,String name,String shape,String geometry){

        String sql2 = "INSERT INTO "+table+" (ID,NAME,FLAG,GEOMETRY,GEOM_) "                                                                            //AND "+table+".GEOMETRY <> \'"+shape+"\'
                + "VALUES (\'"+id+"\', \'"+name+"\',1, \'"+shape+"\',"+geometry+"  ) ON CONFLICT(ID) DO UPDATE SET FLAG=(CASE WHEN ("+table+".NAME <> \'"+name+"\' AND ST_Equals("+table+".GEOM_,"+geometry+") )THEN 1 ELSE 0 END),NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY,GEOM_=EXCLUDED.GEOM_;";
        //System.out.println(sql2);
        return sql2;
    }

    public String Insert_sql2(String table,long id,String name,String shape,String geometry){

        String sql2 = "INSERT INTO "+table+" (ID,NAME,GEOMETRY,GEOM_) "
                + "VALUES (\'"+id+"\', \'"+name+"\', \'"+shape+"\',"+geometry+"  ) ON CONFLICT(ID) DO UPDATE SET NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY,GEOM_=EXCLUDED.GEOM_;";
        //System.out.println(sql2);
        return sql2;
    }




    @Override
    public void process(EntityContainer entityContainer) {
        this.executor.execute(new Runnable() {

            public void run() {
                try {
//                    System.out.println(Thread.currentThread().getName() + "   running!");
//                    Thread.sleep(1000);
                    Statement stmt = null,stmt2 = null;
                    try {
                        stmt = base_connection.createStatement();
                        stmt2 = base_connection.createStatement();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    if (entityContainer instanceof NodeContainer) {
                        Node myNode=((NodeContainer) entityContainer).getEntity();
                        List<Tag> temp_Tag_list= new ArrayList<>(myNode.getTags());
                        String custom_name=multi_level_hash.hash_table_Search(temp_Tag_list,null);
                        String node_name=find_name_tag(myNode.getTags());
//            out.println(myNode.getId());

                        try {
//                stmt = base_connection.createStatement();
                            String geometry="ST_GeomFromText('POINT("+myNode.getLatitude()+" "+myNode.getLongitude()+")',4326)";
                            String sql2 =Insert_sql2("osm_all_data",myNode.getId(),node_name,"shmeio",geometry);

//                    if(way_name!=null){
//                        sql2=sql2+"NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY;";
//                    }
//                    System.out.println(sql2);
                            stmt.executeUpdate(sql2);
//                stmt.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.err.println(e.getClass().getName()+": "+e.getMessage());
                            System.exit(11);
                        }

                        if(custom_name!=null){
//                System.out.println(myNode.getId()+" "+Arrays.toString(myNode.getTags().toArray())+"==>"+custom_name);
//                System.out.println("--"+myNode.getLatitude()+","+myNode.getLongitude());

                            try {
                                //stmt = base_connection.createStatement();
                                String geometry="ST_GeomFromText('POINT("+myNode.getLatitude()+" "+myNode.getLongitude()+")',4326)";
                                String sql2 = Insert_sql(custom_name,"node/"+myNode.getId(),node_name,"shmeio",geometry);


//                    System.out.println(sql2);
                                stmt.executeUpdate(sql2);
                                //stmt.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.err.println(e.getClass().getName()+": "+e.getMessage());
                                System.exit(11);
                            }

                        }

                    }
                    else if (entityContainer instanceof WayContainer) {
//            System.out.println("HELLO");
                        String geometry=null;
                        //Statement stmt = null;
                        Way myWay = ((WayContainer) entityContainer).getEntity();
                        List<Tag> temp_Tag_list= new ArrayList<>(myWay.getTags());
//            out.println("--"+myWay.getId());
                        String way_name=find_name_tag(myWay.getTags());
//            System.out.println("--"+Arrays.toString(temp_Tag_list.toArray()));
                        String custom_name=multi_level_hash.hash_table_Search(temp_Tag_list,null);
//            out.println(custom_name);
                        List<WayNode> temp_way_list=myWay.getWayNodes();

                        boolean is_line=false;
                        if(temp_way_list.get(0).getLatitude()==temp_way_list.get(temp_way_list.size()-1).getLatitude() && temp_way_list.get(0).getLongitude()==temp_way_list.get(temp_way_list.size()-1).getLongitude() && temp_way_list.size()>=4){
                            geometry="ST_GeomFromText('POLYGON((";

                        }
                        else{
                            is_line=true;
                            geometry="ST_GeomFromText('LINESTRING(";

                        }
                        if(temp_way_list.size()==1){
                            is_line=true;
                            geometry="ST_GeomFromText('POINT(";
                        }
                        for(int i=0;i<temp_way_list.size();i++){
//                    out.printf("[%.8f,%.8f]",myWay.getWayNodes().get(i).getLatitude(),myWay.getWayNodes().get(i).getLongitude());
                            geometry=geometry+temp_way_list.get(i).getLatitude()+" "+temp_way_list.get(i).getLongitude()+",";
//                    out.print("[La= "+temp_way_list.get(i).getLatitude()+", Lo= "+temp_way_list.get(i).getLongitude()+" ]");
                        }

                        geometry= geometry.substring(0, geometry.length() - 1);
                        if(is_line){
                            geometry=geometry+")',4326)";
                        }
                        else{
                            geometry=geometry+"))',4326)";
                        }
                        try {
                            //stmt = base_connection.createStatement();
                            String sql2=null;
                            if(is_line){
                                sql2 = Insert_sql2("osm_all_data",myWay.getId(),way_name,"GRAMMH",geometry);

                            }
                            else{
                                sql2 = Insert_sql2("osm_all_data",myWay.getId(),way_name,"POLIGONO",geometry);

                            }

                            stmt.executeUpdate(sql2);
                            //stmt.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.err.println(e.getClass().getName()+": "+e.getMessage());
                            System.exit(11);
                        }
                        if(custom_name!=null){
                            //System.out.println(myWay.getId()+" "+Arrays.toString(myWay.getTags().toArray())+"==>"+custom_name);





//                out.println();
//                out.println("GEOMETRY===="+geometry);
//                out.println(Arrays.toString(myWay.getWayNodes().toArray()));



                            try {
                                //stmt = base_connection.createStatement();
                                String sql2=null;
                                if(is_line){
                                    sql2 = Insert_sql(custom_name,"way/"+myWay.getId(),way_name,"GRAMMH",geometry);

                                }
                                else{
                                    sql2 = Insert_sql(custom_name,"way/"+myWay.getId(),way_name,"POLIGONO",geometry);

                                }

//                    String sql2 = "INSERT INTO "+custom_name+" (ID,NAME,GEOMETRY,GEOM_) "
//                            + "VALUES (\'"+myWay.getId()+"\', \'"+way_name+"\', 'GRAMMH',"+geometry+" ) ON CONFLICT(ID) DO UPDATE SET NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY,GEOM_=EXCLUDED.GEOM_;";


//                    if(way_name!=null){
//                        sql2=sql2+"NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY;";
//                    }
                                stmt.executeUpdate(sql2);
                                //stmt.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.err.println(e.getClass().getName()+": "+e.getMessage());
                                System.exit(11);
                            }

                        }
//            for ( Tag myTag : myWay.getTags()) {
//                String name=null;
//                int flag=0;
////                System.out.println(myTag.toString());
//                if ("highway".equalsIgnoreCase(myTag.getKey()) ) {
////                    System.out.println(Arrays.toString(myWay.getTags().toArray()));
////                    System.out.println("Name: "+name);
////                    System.out.println(" Woha, it's a highway: " + myWay.getId()+" "+myWay.toString());
//                    flag=1;
//                }
//                if (myTag.getKey() != null && myTag.getKey().equalsIgnoreCase("name")){
////                    System.out.println("Name: "+myTag.getValue());
////
////                    name= myTag.getValue();
//                }
//
//            }
                    } else if (entityContainer instanceof RelationContainer) {
                        // Nothing to do here
                        Relation myRelation = ((RelationContainer) entityContainer).getEntity();
                        List<Tag> temp_Tag_list= new ArrayList<>(myRelation.getTags());
                        String custom_name=multi_level_hash.hash_table_Search(temp_Tag_list,null);
                        //Statement stmt = null,stmt2=null;
                        if(custom_name!=null){
                            boolean is_coll=false;
                            List<RelationMember> list_members=myRelation.getMembers();
//                System.out.println(myRelation.getId()+" "+Arrays.toString(myRelation.getTags().toArray())+"==>"+custom_name);
//                out.println(myRelation.toString());
//                out.println(list_members);
                            String relation_name=find_name_tag(myRelation.getTags());
                            WKTReader wkt = new WKTReader();
                            LineMerger merger = new LineMerger();
                            try {
                                //stmt = base_connection.createStatement();
                                List<Way_node> all_collection = new ArrayList<Way_node>();
                                for(int i=0;i<list_members.size();i++){
                                    //collection_members++;
                                    long _id=list_members.get(i).getMemberId();
                                    String sql="SELECT id,name,geometry,ST_AsText(geom_) FROM osm_all_data WHERE id ="+_id;
                                    ResultSet rs = stmt.executeQuery(sql);
                                    int size =0;

                                    while(rs.next()){
                                        //collection_members_found++;
                                        Way_node coll_obj=new Way_node(_id,rs.getString("name"),rs.getString("geometry"),rs.getString(4));
                                        all_collection.add(coll_obj);
                                        //collection_members_with_more_than_1_id++;
                                    }
                                    //collection_members_with_more_than_1_id--;

//                    out.printf("[%.8f,%.8f]",myWay.getWayNodes().get(i).getLatitude(),myWay.getWayNodes().get(i).getLongitude());
//                        geometry=geometry+list_members.get(i).getLatitude()+" "+list_members.get(i).getLongitude()+",";
//                    out.print("[La= "+temp_way_list.get(i).getLatitude()+", Lo= "+temp_way_list.get(i).getLongitude()+" ]");
                                }

                                Collection<Geometry> geometries = new ArrayList();
                                Collection<Geometry> polygon_geometries = new ArrayList();
                                for(int i = 0; i < all_collection.size(); i++) {

                                    Geometry geo=wkt.read(all_collection.get(i).geometry);

                                    if(geo.getGeometryType().equalsIgnoreCase("LINESTRING")){
                                        merger.add(geo);
                                    }
                                    else if(geo.getGeometryType().equalsIgnoreCase("POLYGON")){
                                        polygon_geometries.add(geo);
                                    }
                                    else{
                                        is_coll=true;
                                        geometries.add(geo);
                                    }

                                    //System.out.println("ID=="+all_collection.get(i).id+" NAME=="+all_collection.get(i).name+" SHAPE=="+all_collection.get(i).shape+" GEOMETRIA=="+all_collection.get(i).geometry);
                                }
                                //System.out.println("lines->polygons");
                                Collection<LineString> merged_lines=merger.getMergedLineStrings();
                                for (LineString g : merged_lines) {
                                    if(g.isRing()){

                                        GeometryFactory geometryFactory = new GeometryFactory();
                                        Polygon polygonFromCoordinates = geometryFactory.createPolygon(g.getCoordinates());
                                        polygon_geometries.add(polygonFromCoordinates);
                                    }
                                    else{
                                        is_coll=true;
                                        geometries.add(g);

                                    }
                                }
//                    Collection<Geometry> combinedStream = Stream.concat(merged_lines.stream(), geometries.stream()).collect(Collectors.toCollection(TreeSet::new));
                                //System.out.println("MERGING");
                                Geometry merged_geos=null;
                                if(!polygon_geometries.isEmpty()){
                                    merged_geos=wkt.read( polygon_geometries.toArray()[0].toString());
                                    if(polygon_geometries.size()>1){
                                        for(int i = 1; i < polygon_geometries.size(); i++) {
                                            Geometry temp_geo=wkt.read( polygon_geometries.toArray()[i].toString());
                                            System.out.println(polygon_geometries.toArray()[i]);
                                            merged_geos=merged_geos.symDifference(temp_geo);
                                        }
                                    }
                                }

//                    System.out.println("MERGED GEOMETRY");
//                    System.out.println(merged_geos);
                                Geometry combined_geo;
                                if(is_coll){
                                    if(merged_geos!=null){
                                        geometries.add(merged_geos);
                                    }

                                    GeometryFactory gf = new GeometryFactory();
//                        GeometryCollection collection = gf.createGeometryCollection(geometries.toArray(new Geometry[] {}));
                                    combined_geo =gf.buildGeometry(geometries);
//                        GeometryCombiner gm=new GeometryCombiner(geometries);
//                      combined_geo=gm.combine();

                                }
                                else{
                                    combined_geo=merged_geos;
                                }

//                    System.out.println("combined_geo GEOMETRY");
//                    System.out.println(combined_geo);

                                String geo_type;
                                if(combined_geo!=null){
                                    geo_type=combined_geo.getGeometryType();

                                    String shape;
                                    if(geo_type.equalsIgnoreCase("LINESTRING")){
                                        shape="GRAMMH";
                                    }
                                    else if(geo_type.equalsIgnoreCase("POLYGON")){
                                        shape="POLIGONO";
                                    }
                                    else if(geo_type.equalsIgnoreCase("POINT")){
                                        shape="shmeio";
                                    }
                                    else{
                                        shape="MULTY";
                                    }
                                    String geo_str="ST_GeomFromText('"+combined_geo.toString()+"',4326)";
                                    String sql = Insert_sql(custom_name,"relation/"+myRelation.getId(),relation_name,shape,geo_str);
                                    //stmt2 = base_connection.createStatement();
                                    stmt2.executeUpdate(sql);
//                    sql = Insert_sql2("osm_all_data",myRelation.getId(),relation_name,shape,geo_str);
//                    stmt2.executeUpdate(sql);
                                }
                                else{
                                    String sql = Insert_sql(custom_name,"relation/"+myRelation.getId(),relation_name,null,null);
                                    //stmt2 = base_connection.createStatement();
                                    stmt2.executeUpdate(sql);
                                    geo_type="tipota";
                                }

                            } catch (SQLException | ParseException throwables) {
                                throwables.printStackTrace();
                            }
                        }

                    } else {
                        System.out.println("Unknown Entity!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
//        Statement stmt = null,stmt2 = null;
//        try {
//            stmt = base_connection.createStatement();
//            stmt2 = base_connection.createStatement();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        if (entityContainer instanceof NodeContainer) {
//            // Nothing to do here
//
//            Node myNode=((NodeContainer) entityContainer).getEntity();
//            List<Tag> temp_Tag_list= new ArrayList<>(myNode.getTags());
//            String custom_name=this.multi_level_hash.hash_table_Search(temp_Tag_list,null);
//            String node_name=find_name_tag(myNode.getTags());
////            out.println(myNode.getId());
//
//            try {
////                stmt = base_connection.createStatement();
//                String geometry="ST_GeomFromText('POINT("+myNode.getLatitude()+" "+myNode.getLongitude()+")',4326)";
//                String sql2 =Insert_sql2("osm_all_data",myNode.getId(),node_name,"shmeio",geometry);
//
////                    if(way_name!=null){
////                        sql2=sql2+"NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY;";
////                    }
////                    System.out.println(sql2);
//                stmt.executeUpdate(sql2);
////                stmt.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//                System.err.println(e.getClass().getName()+": "+e.getMessage());
//                System.exit(11);
//            }
//
//            if(custom_name!=null){
////                System.out.println(myNode.getId()+" "+Arrays.toString(myNode.getTags().toArray())+"==>"+custom_name);
////                System.out.println("--"+myNode.getLatitude()+","+myNode.getLongitude());
//
//                try {
//                    //stmt = base_connection.createStatement();
//                    String geometry="ST_GeomFromText('POINT("+myNode.getLatitude()+" "+myNode.getLongitude()+")',4326)";
//                    String sql2 = Insert_sql(custom_name,"node/"+myNode.getId(),node_name,"shmeio",geometry);
//
//
////                    System.out.println(sql2);
//                    stmt.executeUpdate(sql2);
//                    //stmt.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.err.println(e.getClass().getName()+": "+e.getMessage());
//                    System.exit(11);
//                }
//
//            }
//
//        } else if (entityContainer instanceof WayContainer) {
////            System.out.println("HELLO");
//            String geometry=null;
//            //Statement stmt = null;
//            Way myWay = ((WayContainer) entityContainer).getEntity();
//            List<Tag> temp_Tag_list= new ArrayList<>(myWay.getTags());
////            out.println("--"+myWay.getId());
//            String way_name=find_name_tag(myWay.getTags());
////            System.out.println("--"+Arrays.toString(temp_Tag_list.toArray()));
//            String custom_name=this.multi_level_hash.hash_table_Search(temp_Tag_list,null);
////            out.println(custom_name);
//            List<WayNode> temp_way_list=myWay.getWayNodes();
//
//            boolean is_line=false;
//            if(temp_way_list.get(0).getLatitude()==temp_way_list.get(temp_way_list.size()-1).getLatitude() && temp_way_list.get(0).getLongitude()==temp_way_list.get(temp_way_list.size()-1).getLongitude() && temp_way_list.size()>=4){
//                geometry="ST_GeomFromText('POLYGON((";
//
//            }
//            else{
//                is_line=true;
//                geometry="ST_GeomFromText('LINESTRING(";
//
//            }
//            if(temp_way_list.size()==1){
//                is_line=true;
//                geometry="ST_GeomFromText('POINT(";
//            }
//            for(int i=0;i<temp_way_list.size();i++){
////                    out.printf("[%.8f,%.8f]",myWay.getWayNodes().get(i).getLatitude(),myWay.getWayNodes().get(i).getLongitude());
//                geometry=geometry+temp_way_list.get(i).getLatitude()+" "+temp_way_list.get(i).getLongitude()+",";
////                    out.print("[La= "+temp_way_list.get(i).getLatitude()+", Lo= "+temp_way_list.get(i).getLongitude()+" ]");
//            }
//
//            geometry= geometry.substring(0, geometry.length() - 1);
//            if(is_line){
//                geometry=geometry+")',4326)";
//            }
//            else{
//                geometry=geometry+"))',4326)";
//            }
//            try {
//                //stmt = base_connection.createStatement();
//                String sql2=null;
//                if(is_line){
//                    sql2 = Insert_sql2("osm_all_data",myWay.getId(),way_name,"GRAMMH",geometry);
//
//                }
//                else{
//                    sql2 = Insert_sql2("osm_all_data",myWay.getId(),way_name,"POLIGONO",geometry);
//
//                }
//
//                stmt.executeUpdate(sql2);
//                //stmt.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//                System.err.println(e.getClass().getName()+": "+e.getMessage());
//                System.exit(11);
//            }
//            if(custom_name!=null){
//                //System.out.println(myWay.getId()+" "+Arrays.toString(myWay.getTags().toArray())+"==>"+custom_name);
//
//
//
//
//
////                out.println();
////                out.println("GEOMETRY===="+geometry);
////                out.println(Arrays.toString(myWay.getWayNodes().toArray()));
//
//
//
//                try {
//                    //stmt = base_connection.createStatement();
//                    String sql2=null;
//                    if(is_line){
//                         sql2 = Insert_sql(custom_name,"way/"+myWay.getId(),way_name,"GRAMMH",geometry);
//
//                    }
//                    else{
//                         sql2 = Insert_sql(custom_name,"way/"+myWay.getId(),way_name,"POLIGONO",geometry);
//
//                    }
//
////                    String sql2 = "INSERT INTO "+custom_name+" (ID,NAME,GEOMETRY,GEOM_) "
////                            + "VALUES (\'"+myWay.getId()+"\', \'"+way_name+"\', 'GRAMMH',"+geometry+" ) ON CONFLICT(ID) DO UPDATE SET NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY,GEOM_=EXCLUDED.GEOM_;";
//
//
////                    if(way_name!=null){
////                        sql2=sql2+"NAME=EXCLUDED.NAME,GEOMETRY=EXCLUDED.GEOMETRY;";
////                    }
//                    stmt.executeUpdate(sql2);
//                    //stmt.close();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.err.println(e.getClass().getName()+": "+e.getMessage());
//                    System.exit(11);
//                }
//
//            }
////            for ( Tag myTag : myWay.getTags()) {
////                String name=null;
////                int flag=0;
//////                System.out.println(myTag.toString());
////                if ("highway".equalsIgnoreCase(myTag.getKey()) ) {
//////                    System.out.println(Arrays.toString(myWay.getTags().toArray()));
//////                    System.out.println("Name: "+name);
//////                    System.out.println(" Woha, it's a highway: " + myWay.getId()+" "+myWay.toString());
////                    flag=1;
////                }
////                if (myTag.getKey() != null && myTag.getKey().equalsIgnoreCase("name")){
//////                    System.out.println("Name: "+myTag.getValue());
//////
//////                    name= myTag.getValue();
////                }
////
////            }
//        } else if (entityContainer instanceof RelationContainer) {
//            // Nothing to do here
//            Relation myRelation = ((RelationContainer) entityContainer).getEntity();
//            List<Tag> temp_Tag_list= new ArrayList<>(myRelation.getTags());
//            String custom_name=this.multi_level_hash.hash_table_Search(temp_Tag_list,null);
//            //Statement stmt = null,stmt2=null;
//            if(custom_name!=null){
//                boolean is_coll=false;
//                List<RelationMember> list_members=myRelation.getMembers();
////                System.out.println(myRelation.getId()+" "+Arrays.toString(myRelation.getTags().toArray())+"==>"+custom_name);
////                out.println(myRelation.toString());
////                out.println(list_members);
//                String relation_name=find_name_tag(myRelation.getTags());
//                WKTReader wkt = new WKTReader();
//                LineMerger merger = new LineMerger();
//                try {
//                    //stmt = base_connection.createStatement();
//                    List<Way_node> all_collection = new ArrayList<Way_node>();
//                    for(int i=0;i<list_members.size();i++){
//                        collection_members++;
//                        long _id=list_members.get(i).getMemberId();
//                        String sql="SELECT id,name,geometry,ST_AsText(geom_) FROM osm_all_data WHERE id ="+_id;
//                        ResultSet rs = stmt.executeQuery(sql);
//                        int size =0;
//
//                        while(rs.next()){
//                            collection_members_found++;
//                            Way_node coll_obj=new Way_node(_id,rs.getString("name"),rs.getString("geometry"),rs.getString(4));
//                            all_collection.add(coll_obj);
//                            collection_members_with_more_than_1_id++;
//                        }
//                        collection_members_with_more_than_1_id--;
//
////                    out.printf("[%.8f,%.8f]",myWay.getWayNodes().get(i).getLatitude(),myWay.getWayNodes().get(i).getLongitude());
////                        geometry=geometry+list_members.get(i).getLatitude()+" "+list_members.get(i).getLongitude()+",";
////                    out.print("[La= "+temp_way_list.get(i).getLatitude()+", Lo= "+temp_way_list.get(i).getLongitude()+" ]");
//                    }
//
//                    Collection<Geometry> geometries = new ArrayList();
//                    Collection<Geometry> polygon_geometries = new ArrayList();
//                    for(int i = 0; i < all_collection.size(); i++) {
//
//                        Geometry geo=wkt.read(all_collection.get(i).geometry);
//
//                        if(geo.getGeometryType().equalsIgnoreCase("LINESTRING")){
//                            merger.add(geo);
//                        }
//                        else if(geo.getGeometryType().equalsIgnoreCase("POLYGON")){
//                            polygon_geometries.add(geo);
//                        }
//                        else{
//                            is_coll=true;
//                            geometries.add(geo);
//                        }
//
//                        System.out.println("ID=="+all_collection.get(i).id+" NAME=="+all_collection.get(i).name+" SHAPE=="+all_collection.get(i).shape+" GEOMETRIA=="+all_collection.get(i).geometry);
//                    }
//                    //System.out.println("lines->polygons");
//                    Collection<LineString> merged_lines=merger.getMergedLineStrings();
//                    for (LineString g : merged_lines) {
//                        if(g.isRing()){
//
//                            GeometryFactory geometryFactory = new GeometryFactory();
//                            Polygon polygonFromCoordinates = geometryFactory.createPolygon(g.getCoordinates());
//                            polygon_geometries.add(polygonFromCoordinates);
//                        }
//                        else{
//                            is_coll=true;
//                            geometries.add(g);
//
//                        }
//                    }
////                    Collection<Geometry> combinedStream = Stream.concat(merged_lines.stream(), geometries.stream()).collect(Collectors.toCollection(TreeSet::new));
//                    //System.out.println("MERGING");
//                    Geometry merged_geos=null;
//                    if(!polygon_geometries.isEmpty()){
//                        merged_geos=wkt.read( polygon_geometries.toArray()[0].toString());
//                        if(polygon_geometries.size()>1){
//                            for(int i = 1; i < polygon_geometries.size(); i++) {
//                                Geometry temp_geo=wkt.read( polygon_geometries.toArray()[i].toString());
//                                System.out.println(polygon_geometries.toArray()[i]);
//                                merged_geos=merged_geos.symDifference(temp_geo);
//                            }
//                        }
//                    }
//
////                    System.out.println("MERGED GEOMETRY");
////                    System.out.println(merged_geos);
//                    Geometry combined_geo;
//                    if(is_coll){
//                        if(merged_geos!=null){
//                            geometries.add(merged_geos);
//                        }
//
//                        GeometryFactory gf = new GeometryFactory();
////                        GeometryCollection collection = gf.createGeometryCollection(geometries.toArray(new Geometry[] {}));
//                        combined_geo =gf.buildGeometry(geometries);
////                        GeometryCombiner gm=new GeometryCombiner(geometries);
////                      combined_geo=gm.combine();
//
//                    }
//                    else{
//                        combined_geo=merged_geos;
//                    }
//
////                    System.out.println("combined_geo GEOMETRY");
////                    System.out.println(combined_geo);
//
//                    String geo_type;
//                    if(combined_geo!=null){
//                        geo_type=combined_geo.getGeometryType();
//
//                    String shape;
//                    if(geo_type.equalsIgnoreCase("LINESTRING")){
//                        shape="GRAMMH";
//                    }
//                    else if(geo_type.equalsIgnoreCase("POLYGON")){
//                        shape="POLIGONO";
//                    }
//                    else if(geo_type.equalsIgnoreCase("POINT")){
//                        shape="shmeio";
//                    }
//                    else{
//                        shape="MULTY";
//                    }
//                    String geo_str="ST_GeomFromText('"+combined_geo.toString()+"',4326)";
//                    String sql = Insert_sql(custom_name,"relation/"+myRelation.getId(),relation_name,shape,geo_str);
//                    //stmt2 = base_connection.createStatement();
//                    stmt2.executeUpdate(sql);
////                    sql = Insert_sql2("osm_all_data",myRelation.getId(),relation_name,shape,geo_str);
////                    stmt2.executeUpdate(sql);
//                    }
//                    else{
//                        String sql = Insert_sql(custom_name,"relation/"+myRelation.getId(),relation_name,null,null);
//                        //stmt2 = base_connection.createStatement();
//                        stmt2.executeUpdate(sql);
//                        geo_type="tipota";
//                    }
//                    //stmt2.close();
////                    Geometry merged_test=wkt.read("POLYGON((0 0, 0 5, 5 5,5 0,0 0))");
////                    Geometry temp_test=wkt.read("POLYGON((1 1, 1 4, 4 4,4 1,1 1))");
////                    Geometry temp_test2=wkt.read("POLYGON((0 7, 0 8, 2 8,2 7,0 7))");
////                    Collection<Geometry> geometries2 = new ArrayList();
////                    geometries2.add(merged_test);
////                    geometries2.add(temp_test);
////                    merged_test=merged_test.symDifference(temp_test);
////                    merged_test=merged_test.symDifference(temp_test2);
////                    //geometries2.add(temp_test2);
////                    GeometryFactory gf2 = new GeometryFactory();
////                    Geometry combined_geo2 =gf2.buildGeometry(geometries2);
////
////                    System.out.println(merged_test);
////                    System.out.println(combined_geo2);
//                    //System.out.println(geometries);
//
//                } catch (SQLException | ParseException throwables) {
//                    throwables.printStackTrace();
//                }
//            }
//
//        } else {
//            System.out.println("Unknown Entity!");
//        }
//        try {
//            stmt2.close();
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
    }

    @Override
    public void complete() {
    }

    @Override
    public void close() {
    }

//    public int random_hash_function(String st){
//        int hash = 7;
//        for (int i = 0; i < st.length(); i++) {
//            hash = hash*31 + st.charAt(i);
//        }
//        return hash;
//    }

}

//public class MyOsmReader {
//    public static void main(String[] args) {
//        System.out.println("HELLO");
//    }
//}
