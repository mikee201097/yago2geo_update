package com.tests;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.riot.system.StreamRDF;
import org.apache.jena.riot.system.StreamRDFWrapper;

public class FilterSinkRDF extends StreamRDFWrapper {
    private final Node[] properties;

    public FilterSinkRDF(StreamRDF other, Property...properties) {
        super(other);
        this.properties = new Node[properties.length];
        for ( int i = 0 ; i < properties.length ; i++ )
            this.properties[i] = properties[i].asNode();
    }



    @Override
    public void triple(Triple triple) {
        for ( Node p : properties ) {
            if ( triple.getPredicate().equals(p) )
                super.triple(triple);
        }
    }
}


