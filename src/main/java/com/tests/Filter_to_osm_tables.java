package com.tests;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class Filter_to_osm_tables {

    private Connection base_con;

    String query_creation(String table,String[] tags,String line_poly){
        String sql="INSERT INTO "+table+" (ID,NAME,FLAG,GEOMETRY,GEOM_) SELECT CASE WHEN(osm_id>0) THEN CONCAT('way/',ABS(osm_id)::text) ELSE CONCAT('relation/',ABS(osm_id)::text) END as osm_id1,name,1,'grammh',way "
            +"from "+line_poly+" WHERE";
        for(String i:tags){
            String[] key_val = i.split(":");
            sql=sql+" "+line_poly+"."+key_val[0]+"='"+key_val[1]+"' AND";
        }
            //+line_poly+".natural='water' AND "+line_poly+".water='lake' "
        sql=sql.substring(0,sql.lastIndexOf(" "));
        sql=sql+" ON CONFLICT(ID) DO UPDATE SET FLAG=(CASE WHEN ( ST_Equals("+table+".GEOM_,EXCLUDED.GEOM_) )THEN 0 ELSE 1 END),NAME=EXCLUDED.NAME,GEOM_=EXCLUDED.GEOM_;";
        return sql;
    }

    Filter_to_osm_tables(Connection a, Hashtable<String[],String> cat_tags) throws SQLException {
        this.base_con=a;
        Statement stmt = this.base_con.createStatement();
        Statement stmt2 = this.base_con.createStatement();
        Set<String> value_set = new HashSet<String>(cat_tags.values());
        cat_tags.forEach((k,v)->{
            String query=query_creation(v,k,"planet_osm_polygon");
            System.out.println(query);
            try {
                stmt.executeUpdate(query);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            query=query_creation(v,k,"planet_osm_line");
            System.out.println(query);
            try {
                stmt2.executeUpdate(query);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
//        for(String osm_name:value_set){
//            String sql="INSERT INTO "+osm_name+" (ID,NAME,FLAG,GEOMETRY,GEOM_) SELECT CASE WHEN(osm_id>0) THEN CONCAT('way/',ABS(osm_id)::text) ELSE CONCAT('relation/',ABS(osm_id)::text) END as osm_id1,name,1,'grammh',way "
//                    +"from planet_osm_polygon WHERE planet_osm_polygon.natural='water' AND planet_osm_polygon.water='lake' "
//                    +"ON CONFLICT(ID) DO UPDATE SET FLAG=(CASE WHEN ( ST_Equals("+osm_name+".GEOM_,EXCLUDED.GEOM_) )THEN 0 ELSE 1 END),NAME=EXCLUDED.NAME,GEOM_=EXCLUDED.GEOM_;";


//            stmt.executeUpdate(sql);
//
//        }
        stmt.close();
        stmt2.close();
    }
}
