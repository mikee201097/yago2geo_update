package com.tests;

import org.openstreetmap.osmosis.pbf2.v0_6.PbfReader;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.concurrent.*;
import java.util.function.Supplier;

import static java.lang.System.out;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("HELLO");
        Input_file_reader conf_file=new Input_file_reader(args[0]);
        LinkedHashMap<String, String> conf_info=conf_file.get_base_info();
//        for (String key : conf_info.keySet()) {
//            out.println("Key: "+key+" : "+conf_info.get(key));
//        }
        Connection_Manager base_connection=new Connection_Manager(conf_info);
        Connection actual_connection= base_connection.get_Connection();
        Hashtable<String[], String> all_catname_tags=conf_file.get_all_tags();

        Database_table_creation tables_cr=new Database_table_creation(actual_connection,all_catname_tags);
        for(String[] k:all_catname_tags.keySet()){
            out.println("Key: "+Arrays.toString(k)+" Value: "+all_catname_tags.get(k));
        }
        Tags_hashtable multi_hash=new Tags_hashtable(all_catname_tags);
        multi_hash.print_hash();
        long start = System.nanoTime();
        String base_name = conf_info.get("base_name");

        Statement stmt = actual_connection.createStatement();
        String sql="SELECT EXISTS ( SELECT FROM information_schema.tables WHERE table_name='planet_osm_line') "
                +"AND EXISTS ( SELECT FROM information_schema.tables WHERE table_name='planet_osm_polygon')"
                +"AND EXISTS ( SELECT FROM information_schema.tables WHERE table_name='planet_osm_nodes')"
                +"AND EXISTS ( SELECT FROM information_schema.tables WHERE table_name='planet_osm_ways')"
                +"AND EXISTS ( SELECT FROM information_schema.tables WHERE table_name='planet_osm_rels')";
        ResultSet rs = stmt.executeQuery(sql);
        String create_append="create";
        while(rs.next()){
            String if_exists = rs.getString(1);
            if(if_exists.equalsIgnoreCase("t")){
                create_append="append";
            }
        }
        out.println(create_append);
        sql="SELECT EXISTS ( SELECT FROM information_schema.tables WHERE table_name='yago_all_data')";
        rs = stmt.executeQuery(sql);
        boolean create_yago=true;
        while(rs.next()){
            String if_exists = rs.getString(1);
            if(if_exists.equalsIgnoreCase("t")){
                create_yago=false;
            }
        }
        String path="/home/user-sl/Documents/PTYXIAKH/testing-tools/central-america-latest";
        String new_file=conf_info.get("Osm_pbf_path").replace(".osm.pbf","_new.osm.pbf");
        String osmupdate_cmd=conf_info.get("osmuppdate_dir")+"/osmupdate --base-url=download.geofabrik.de/europe/greece-updates "+conf_info.get("Osm_pbf_path")+" 2020-10-15T23:30:00Z changes.osc.gz -v";
        String osmconvert="osmconvert "+conf_info.get("Osm_pbf_path")+" changes.osc.gz -o="+new_file;
        String osm2pgsql_cmd="osm2pgsql --create --slim -l --number-processes 3 --cache 240 --multi-geometry --style "+conf_info.get("osm2psql_style_path")+" -U postgres -W -d "+base_name+" "+new_file;
        String osm2pgsql_cmd2="osm2pgsql --append --slim -l --number-processes 3 --cache 240 --multi-geometry --style "+conf_info.get("osm2psql_style_path")+" -U postgres -W -d "+base_name+" changes.osc.gz";
        String overwrite_cmd="mv "+new_file+" "+conf_info.get("Osm_pbf_path");
        try {
            //osmupdate
            System.out.println(osmupdate_cmd);
            Process process1 = Runtime.getRuntime().exec(osmupdate_cmd);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process1.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process1.getErrorStream()));

            System.out.println("Here is the standard output of the command osmupdate:\n");
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
            System.out.println("Here is the standard error of the command (if any) osmupdate:\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
            process1.waitFor();
            //osmconvert
            System.out.println(osmupdate_cmd);
            Process process2 = Runtime.getRuntime().exec(osmconvert);
            stdInput = new BufferedReader(new InputStreamReader(process2.getInputStream()));
            stdError = new BufferedReader(new InputStreamReader(process2.getErrorStream()));
            System.out.println("Here is the standard output of the command osmconvert:\n");
            s = null;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
            System.out.println("Here is the standard error of the command (if any) osmconvert:\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
            process2.waitFor();

            System.out.println(overwrite_cmd);
            Process process4 = Runtime.getRuntime().exec(overwrite_cmd);
            process4.waitFor();
            //osm2psql
            Process process3;

            if(create_append.equals("create")){
                System.out.println(osm2pgsql_cmd);
                process3 = Runtime.getRuntime().exec(osm2pgsql_cmd);
            }
            else{
                System.out.println(osm2pgsql_cmd2);
                process3 = Runtime.getRuntime().exec(osm2pgsql_cmd2);
            }
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process3.getOutputStream()));
            bw.write(conf_info.get("base_password")+"\n");
            bw.flush();
            process3.getOutputStream().close();
            stdInput = new BufferedReader(new InputStreamReader(process3.getInputStream()));
            stdError = new BufferedReader(new InputStreamReader(process3.getErrorStream()));
            System.out.println("Here is the standard output of the command osm2psql:\n");
            s = null;
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
            System.out.println("Here is the standard error of the command (if any) osm2psql:\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
            bw.close();
            process3.waitFor();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        //Filter_to_osm_tables Ftot=new Filter_to_osm_tables(actual_connection,all_catname_tags);
        out.println("MPLA");
        Filter_to_osm_all_data Ftot=new Filter_to_osm_all_data(actual_connection,all_catname_tags);

//

        if(create_yago){
            MyRdfReader_to_all r= new MyRdfReader_to_all(conf_info.get("yago2geoFile"),actual_connection);
//            MyRdfReader r= new MyRdfReader("/home/user-sl/Documents/PTYXIAKH/yago2geo_osm/OSM_extended.ttl",actual_connection);
            LoadYagoGeos lyg=new LoadYagoGeos(actual_connection,all_catname_tags);
        }
        Yago_tables_update ytu=new Yago_tables_update(actual_connection,all_catname_tags);
        //my_Sparql_update mSu= new my_Sparql_update(actual_connection);
        //apply the updates to the table
        String apply_geom_updates="UPDATE yago_all_data yad1 SET geom_prev=yad2.geom_updated, geom_updated=NULL FROM yago_all_data yad2 WHERE yad1.id=yad2.id AND yad1.geom_updated is not null;";
        String apply_name_updates="UPDATE yago_all_data yad1 SET name=CONCAT('\"',yad2.name_updated,'\"'), name_updated=NULL FROM yago_all_data yad2 WHERE yad1.id=yad2.id AND yad1.name_updated is not null;";
        String apply_cate_updates="UPDATE yago_all_data yad1 SET category=yad2.category_updated, category_updated=NULL FROM yago_all_data yad2 WHERE yad1.id=yad2.id AND yad1.category_updated is not null;";
        System.out.println(apply_geom_updates);
        Statement apply_geom_stmt = actual_connection.createStatement();
        apply_geom_stmt.executeUpdate(apply_geom_updates);
        apply_geom_stmt.close();
        
        System.out.println(apply_name_updates);
        Statement apply_name_stmt = actual_connection.createStatement();
        apply_name_stmt.executeUpdate(apply_name_updates);
        apply_name_stmt.close();

        System.out.println(apply_cate_updates);
        Statement apply_cate_stmt = actual_connection.createStatement();
        apply_cate_stmt.executeUpdate(apply_cate_updates);
        apply_cate_stmt.close();
        long elapsedTime = System.nanoTime() - start;
        out.println("Time: "+elapsedTime/1000);
    }
}
