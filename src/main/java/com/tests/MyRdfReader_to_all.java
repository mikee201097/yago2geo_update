package com.tests;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.system.StreamRDF;
import org.apache.jena.riot.system.StreamRDFBase;
import org.apache.jena.riot.system.StreamRDFLib;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class MyRdfReader_to_all {
    String file_name;
    private Connection base_connection;

    public String Insert_sql(String table,String id,String name,String link,String geometry){
        String sql2 = "INSERT INTO yago_all_data"+" (ID,NAME,CATEGORY,LINK,GEOMETRY) "
                + "VALUES (\'"+id+"\', \'"+name+"\',\'"+table.toLowerCase()+"\', \'"+link+"\',\'"+geometry+"\'  ) ON CONFLICT(ID) DO UPDATE SET NAME=EXCLUDED.NAME,CATEGORY=EXCLUDED.CATEGORY,LINK=EXCLUDED.LINK,GEOMETRY=EXCLUDED.GEOMETRY;";
        return sql2;
    }

    public String Insert_sql_geo(String id,String geometry){
        String sql2 = "INSERT INTO yago_geos (ID,GEOM_) "
                + "VALUES (\'"+id+"\', "+geometry+") ON CONFLICT(ID) DO UPDATE SET GEOM_=EXCLUDED.GEOM_;";
        return sql2;
    }

    Statement stmt;

    String pre_type="http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    String pre_id="http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_ID";
    String pre_name="http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_Name";
    String pre_geo="http://www.opengis.net/ont/geosparql#hasGeometry";
    String pre_as_wkt="http://www.opengis.net/ont/geosparql#asWKT";

    String unwanted_str="\"<http://www.opengis.net/def/crs/EPSG/0/4326> ";
    String unwanted_str2="\"^^http://www.opengis.net/ont/geosparql#wktLiteral";


    MyRdfReader_to_all(String name,Connection con) throws FileNotFoundException, SQLException {
        this.file_name=name;
        this.base_connection=con;
        stmt = base_connection.createStatement();
//        Model model= ModelFactory.createDefaultModel();
//        model.read(new FileInputStream(name),null,"TTL");
//        StmtIterator subjIter = model.listStatements();
        StreamRDF inputHandler = new MyRdfReader_to_all.myCustomeStream();// {
//            @Override
//            public void triple(Triple triple) { // Got a triple
//                Node object = triple.getObject();
//                System.out.println(object.toString());
//                Node transformed;
//                // if object is literal and has wkt type
////                if (object.isLiteral() &&
////                        wkt.equals(object.getLiteralDatatypeURI())) {
////                    // Make a new node, suitably modified
////                    transformed = NodeFactory.createLiteral(
////                            "<http://www.opengis.net/def/crs/EPSG/0/4326> "
////                                    + object.getLiteralLexicalForm(),
////                            object.getLiteralDatatype());
////                } else { // Do nothing
////                    transformed = object;
////                }
//
//                // Write out with corrected object
////                outputHandler.triple(
////                        Triple.create( triple.getSubject(), triple.getPredicate(),
////                                transformed
////                        ));
//            }
//        };
        StreamRDF output= StreamRDFLib.writer(System.out);
        Model M_MODEL = ModelFactory.createDefaultModel();
        StreamRDF filtered = new FilterSinkRDF(output,M_MODEL.createProperty("http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_Name"),M_MODEL.createProperty("http://kr.di.uoa.gr/yago2geo/ontology/hasOSM_ID"),M_MODEL.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type") );
        //StreamRDF filtered = (StreamRDFLib.writer(System.out));
        RDFDataMgr.parse(inputHandler, name);
        stmt.close();
//        while(subjIter.hasNext()) {
//            Statement stmt      = subjIter.nextStatement();  // get next statement
//            Resource  subject   = stmt.getSubject();     // get the subject
//            Property predicate = stmt.getPredicate();   // get the predicate
//            RDFNode object    = stmt.getObject();      // get the object
//
//            System.out.print(subject.toString());
//            System.out.print(" " + predicate.toString() + " ");
//        }

    }

    public class myCustomeStream extends StreamRDFBase {
        //private final Node[] properties;

//        public FilterSinkRDF(StreamRDF other, Property...properties) {
//            super(other);
//            this.properties = new Node[properties.length];
//            for ( int i = 0 ; i < properties.length ; i++ )
//                this.properties[i] = properties[i].asNode();
//        }

        HashMap<String, String> map = new HashMap<>();

        @Override
        public void triple(Triple triple) { // Got a triple
            String object = triple.getObject().toString();
            String subject = triple.getSubject().toString();
            String predicate = triple.getPredicate().toString();
            //System.out.println(triple.toString());
            Node transformed;
            if(predicate.equals(pre_type)){
                map = new HashMap<>();
                map.put("name_link",subject.substring(subject.lastIndexOf("/") + 1).replace("\'","\'\'"));
                map.put("table",object.substring(object.lastIndexOf("/") + 1));
            }
            if(predicate.equals(pre_id)){
                map.put("id",object.replace("\"",""));           //object.substring(object.substring(0, object.lastIndexOf("/")).lastIndexOf("/") + 1)
//                System.out.println(triple.getObject().toString());
            }
            if(predicate.equals(pre_name)){
//                System.out.println(triple.getObject().toString());
                String ob_name=map.get("name");
                if(!object.contains("@")){
                    map.put("name",object.replace("\'","\'\'"));
                }
//                if(ob_name!=null){
//                    if(object.contains("@en") && ob_name.contains("@")){
//                        map.put("name",object.replace("\'","\'\'"));
//                    }
//                    else if(!object.contains("@en")){
//                        map.put("name",object.replace("\'","\'\'"));        //replace("\"","")
//                    }
//                }
//                else{
//                    map.put("name",object.replace("\'","\'\'"));   //object.substring(object.lastIndexOf("/") + 1)
//                }

            }
            if(predicate.equals(pre_geo)){
                map.put("geo",object.substring(object.lastIndexOf("/") + 1));
//                System.out.println(map.toString());
                try {

                    String sql2 =Insert_sql(map.get("table"),map.get("id"),map.get("name"),map.get("name_link"),map.get("geo"));

//                    System.out.println(sql2);
                    stmt.executeUpdate(sql2);

                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println(e.getClass().getName()+": "+e.getMessage());
                    System.exit(11);
                }

            }

            if(predicate.equals(pre_as_wkt)){
                //map.put("geo_id",object.substring(object.lastIndexOf("/") + 1));
                String geo_id=subject.substring(subject.lastIndexOf("/") + 1);


                String geom_=object.replace(unwanted_str,"").replace(unwanted_str2,"");
                String geometry="ST_GeomFromText('"+geom_+"',4326)";
                System.out.println(geo_id+" "+geometry);
                try {
                    String sql2 =Insert_sql_geo(geo_id,geometry);
//                    System.out.println(sql2);
                    stmt.executeUpdate(sql2);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println(e.getClass().getName()+": "+e.getMessage());
                    System.exit(11);
                }

            }
            // if object is literal and has wkt type
//                if (object.isLiteral() &&
//                        wkt.equals(object.getLiteralDatatypeURI())) {
//                    // Make a new node, suitably modified
//                    transformed = NodeFactory.createLiteral(
//                            "<http://www.opengis.net/def/crs/EPSG/0/4326> "
//                                    + object.getLiteralLexicalForm(),
//                            object.getLiteralDatatype());
//                } else { // Do nothing
//                    transformed = object;
//                }

            // Write out with corrected object
//                outputHandler.triple(
//                        Triple.create( triple.getSubject(), triple.getPredicate(),
//                                transformed
//                        ));
        }
    }
}
